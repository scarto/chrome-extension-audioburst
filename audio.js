﻿var searchOnBing = function () {
    var value = sb_form_q.value;
    var searchUrl = "https://content.audioburst.com/search?value=" + value + "&sort=views";
    $.getJSON(searchUrl, function (data) {
        displayBingResults(data);
    });
};

var displayBingResults = function (results) {

  var burstSrc = "https://az715611.vo.msecnd.net/audio/";

  console.log(results);
  var resultLength = results.length;
  var resultString = (/1$/i.test(resultLength)) && resultLength > 11 && resultLength != '1' ? ' Result' : ' Results';
  $('.sb_count').html(resultLength + resultString);

  var contentHTML = '';
  var tmpHTML = '';
  for (var i = 0; i < results.length; i++) {
    var r = results[i];
    tmpHTML = '<li class="b_ans" data-bm="' + (i+5) + '"><div class="ans_news" name="ans_news">'+
      '<h2><a class="headphones-link" target="_blank" href="https://www.audioburst.com/burst/' + r.burstId + '/" h="ID=news,5053.1"><strong>'+ r.title +'</strong></a></h2>' +
      '<div class="b_attribution"><cite>AudioBurst.com</cite></div>' +
      '<div class="b_rich">'+
      '<ul class="b_vList"><li><div class="b_clearfix b_overflow">' +
      '<div class="b_float_img flex-image"><a target="_blank" href="https://www.audioburst.com/burst/' + r.burstId + '/" target="" h="ID=news,5055.1"><span class="nsVidAlgoImg">' +
      '<img title="' + r.title + '" width="170" height="120" alt="' + r.title + '" id="emb' + (i+5) + '" class="rms_img" src="' + r.images[0].src + '" data-bm="' + (i+5) + '">'+
      '<img class="headphones">' +
      '</span></a></div>'+
      '<div class="b_overflow flex-text"><h5><a target="_blank" href="https://www.audioburst.com/burst/' + r.burstId + '/" target="" h="ID=news,5055.1">' + (r.blurb).replace(/(([^\s]+\s\s*){13})(.*)/,"$1…") + '</a></h5>'+
      '<div class="b_attribution"><cite>' + r.parent.title + '<span class="b_secondaryText">&nbsp;· ' + time_ago(r.createdOn) + '</span></cite></div>'+
      '<p class="snippet">By AudioBurst.com</p>'+
      '<br><audio controls><source src="' + burstSrc + r.burstId + '.mp3"> <p>Your browser does not support native audio</p></audio>' +
      '</div>' +
      '</div></li></ul></div></div></li>';

    contentHTML += tmpHTML;
  }

  $('#b_results').html(contentHTML);

};

var addAudioTabToBing = function () {
  console.log('addAudioTabToBing');
  var newsTabs = document.getElementById("scpt3");
  if (newsTabs != null) {
    console.log("Main Bing Page");
    var audioTab = document.createElement("li");
    audioTab.setAttribute("id", "scptAudio");
    audioTab.innerHTML = '<a href="">Audio</a>';
    newsTabs.parentNode.insertBefore(audioTab, newsTabs.previousSibling);
  } else {
    while ($('.b_scopebar') == null) {
        console.log("Sec Bing Page");
    }
    var mainNav = $('.b_scopebar').children();
    newsTabs = $('.b_scopebar').children().children().eq(3)[0];
    var audioTab = document.createElement("li");
    audioTab.setAttribute("id", "scptAudio");
    audioTab.innerHTML = '<a href="#">Audio</a>';
    var list = document.getElementsByTagName("UL")[0];
    console.log(list)
    list.insertBefore(audioTab, list.children[3]);
  }
};

$(document).ready(function () {
  console.log('DOC Ready');
    if (window.location.href.indexOf("bing.com") > 0 && document.getElementsByTagName("UL").length) {
      addAudioTabToBing();
      $("#scptAudio").click(function () {
        $("#scptAudio").addClass("b_active");
        document.getElementsByTagName("UL")[0].children[0].removeAttribute("class");
        searchOnBing();
      });
    }
});

function time_ago(time){

  switch (typeof time) {
    case 'number': break;
    case 'string': time = +new Date(time); break;
    case 'object': if (time.constructor === Date) time = time.getTime(); break;
    default: time = +new Date();
  }
  var time_formats = [
    [60, 'seconds', 1], // 60
    [120, '1 minute ago', '1 minute from now'], // 60*2
    [3600, 'minutes', 60], // 60*60, 60
    [7200, '1 hour ago', '1 hour from now'], // 60*60*2
    [86400, 'hrs', 3600], // 60*60*24, 60*60
    [172800, 'Yesterday', 'Tomorrow'], // 60*60*24*2
    [604800, 'days', 86400], // 60*60*24*7, 60*60*24
    [1209600, 'Last week', 'Next week'], // 60*60*24*7*4*2
    [2419200, 'weeks', 604800], // 60*60*24*7*4, 60*60*24*7
    [4838400, 'Last month', 'Next month'], // 60*60*24*7*4*2
    [29030400, 'months', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
    [58060800, 'Last year', 'Next year'], // 60*60*24*7*4*12*2
    [2903040000, 'years', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
    [5806080000, 'Last century', 'Next century'], // 60*60*24*7*4*12*100*2
    [58060800000, 'centuries', 2903040000] // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
  ];
  var seconds = (+new Date() - time) / 1000,
    token = 'ago', list_choice = 1;

  if (seconds == 0) {
    return 'Just now'
  }
  if (seconds < 0) {
    seconds = Math.abs(seconds);
    token = 'from now';
    list_choice = 2;
  }
  var i = 0, format;
  while (format = time_formats[i++])
    if (seconds < format[0]) {
      if (typeof format[2] == 'string')
        return format[list_choice];
      else
        return Math.floor(seconds / format[2]) + ' ' + format[1] + ' ' + token;
    }

  return time;
}